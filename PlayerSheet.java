package com.example.myapplication;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;



public class PlayerSheet extends AppCompatActivity {


    /*EditText name, health, strength, dexterity, intelligence, vitality, abilitylist;

    public EditText getName() {
        return name;
    }

    public EditText getHealth() {
        return health;
    }

    public EditText getStrength() {
        return strength;
    }

    public EditText getDexterity() {
        return dexterity;
    }

    public EditText getIntelligence() {
        return intelligence;
    }

    public EditText getVitality() {
        return vitality;
    }

    public EditText getAbilitylist() {
        return abilitylist;
    }*/
    public void onActivityResult( int requestCode, int resultCode, Intent data ){
        super.onActivityResult(requestCode, resultCode, data);
        public static final int PLAYER_SHEET_ACTIVITY_REQUEST_CODE = 1;
        if (requestCode == PLAYER_SHEET_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK){

        }else{
            Toast.makeText(getApplicationContext(), R.string.dexterity, Toast.LENGTH_LONG).show();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_sheet);

        PlayerData.ViewModel mView = ViewModelProviders.of(this).get(PlayerData.ViewModel.class);
        mView.getmAllWords().observe(this, new Observer<List<PlayerData>>() {
            @Override
            public void onChanged(@Nullable List<PlayerData> playerData) {
                adapter.setplayerData();
            }
        });

    }
}
