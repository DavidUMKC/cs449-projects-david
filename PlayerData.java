package com.example.myapplication;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.view.View;

import java.util.List;

@Entity(tableName = "field_table")
public class PlayerData {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "field")
    private String mField;
    public PlayerData(String field) {this.mField = field;}
    public String getField (){ return this.mField;}

    @Dao
    public interface PlayerDataDao{
        @Insert
        void insert (PlayerData field);
        @Query("Delete From field_table")
        void deleteAll();
        @Query("Select * from field_table ORDER by field ASC")
        LiveData<List<PlayerData>> getAllWords();
    }

    @Database(entities = {PlayerSheet.class}, version = 1)
    public abstract class PlayerDatabase extends RoomDatabase {

        public abstract PlayerDataDao PlayerDataDao();
        private static volatile PlayerDatabase INSTANCE;

        static PlayerDatabase getDatabase(final Context context) {
            if (INSTANCE == null) {
                synchronized (PlayerDatabase.class) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.getApplicationContext(), PlayerDatabase.class, "sheet_database").build();
                    }
                }
            }
            return INSTANCE;
        }
    }
    public class Repository{
        private PlayerDataDao mPlayerDataDao;
        private LiveData<List<PlayerData>> mAllWords;

        Repository (Application application){
            PlayerDatabase pdb = PlayerDatabase.getDatabase(application);
            mPlayerDataDao = pdb.PlayerDataDao();
            mAllWords = mPlayerDataDao.getAllWords();
        }
        LiveData<List<PlayerData>> getmAllWords(){
            return mAllWords;
        }
        public void insert (PlayerData Player){
            new insertAsyncTask(mPlayerDataDao).execute(Player);
        }
        private static class insertAsyncTask extends AsyncTask<PlayerData, Void, Void>{
            private PlayerDataDao mAsyncTaskDao;
            insertAsyncTask(PlayerDataDao dao){
                mAsyncTaskDao = dao;
            }
            @Override
            protected Void doInBackGround(final PlayerData... params){
                mAsyncTaskDao.insert(params[0]);
                return null;
            }
        }
    }
    public class ViewModel extends AndroidViewModel{
        private Repository mReposit;
        private LiveData<List<PlayerData>> mAllWords;

        public ViewModel (Application application){
            super(application);
            mReposit = new Repository(application);
            mAllWords = mReposit.getmAllWords();
        }
        LiveData<List<PlayerData>> getmAllWords() { return mAllWords;}
        public void insert (PlayerData Player) { mReposit.insert(Player);}
    }
}

